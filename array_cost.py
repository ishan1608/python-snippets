number_of_tests = int(raw_input())

for i in xrange(number_of_tests):
    numbers_size, cost_factor = map(int, raw_input().split())
    numbers = list(map(int, raw_input().split()))

    total_cost = 0
    previous_number = 0
    for number in numbers:
        number_cost = cost_factor * (number - previous_number)
        if previous_number < number:
            total_cost += number_cost
        else:
            total_cost -= number_cost
        previous_number = number
    print total_cost
