import math


class PaginationHelper:
    # The constructor takes in an array of items and a integer indicating
    # how many items fit within a single page
    def __init__(self, items, items_per_page):
        self.collection = items
        self.items_per_page = items_per_page

    # returns the number of items within the entire collection
    def item_count(self):
        return len(self.collection)

    # returns the number of pages
    def page_count(self):
        return int(math.ceil(self.item_count() / float(self.items_per_page)))

    # returns the number of items on the current page. page_index is zero based
    # this method should return -1 for page_index values that are out of range
    def page_item_count(self, page_index):
        items_count = self.item_count()
        page_fist_item_index = page_index * self.items_per_page

        if (page_fist_item_index < 0) or (page_fist_item_index > items_count):
            return -1

        page_last_item_index = ((page_index + 1) * self.items_per_page) - 1

        if items_count >= page_last_item_index:
            return self.items_per_page

        return items_count - page_fist_item_index

    # determines what page an item is on. Zero based indexes.
    # this method should return -1 for item_index values that are out of range
    def page_index(self, item_index):
        if (item_index < 0) or (item_index >= self.item_count()):
            return -1
        return (item_index + 1) / self.items_per_page


collection = range(1, 25)
helper = PaginationHelper(collection, 10)

print(helper.page_count())  # 3
print(helper.page_index(23))  #2
print(helper.item_count())  #24
print(helper.page_item_count(1))  # 10
print(helper.page_item_count(2))  # 5
