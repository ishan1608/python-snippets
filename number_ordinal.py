def numbertoordinal(number):
    if number == 0:
        return '0'

    def suffix_finder(num):
        suffix_map = {
            1: 'st',
            2: 'nd',
            3: 'rd'
        }
        if num in suffix_map:
            return suffix_map[num]
        return 'th'

    suffix = 'th'  # Default suffix

    num_last_two = number % 100

    if (num_last_two > 0) and (num_last_two < 4):
        suffix = suffix_finder(num_last_two)

    if num_last_two > 20:
        suffix = suffix_finder(num_last_two % 10)

    return '{}{}'.format(number, suffix)
