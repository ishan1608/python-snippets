class Node:
    def __init__(self, character):
        self.character = character
        self.dot = None
        self.dash = None


# Initializing morse code tree
class MorseCode:
    def __init__(self):
        e = Node('E')
        self.dot = e
        i = Node('I')
        e.dot = i
        a = Node('A')
        e.dash = a
        s = Node('S')
        i.dot = s
        u = Node('U')
        i.dash = u
        r = Node('R')
        a.dot = r
        w = Node('W')
        a.dash = w
        n = Node('N')
        t = Node('T')
        self.dash = t
        t.dot = n
        m = Node('M')
        t.dash = m
        d = Node('D')
        n.dot = d
        k = Node('K')
        n.dash = k
        g = Node('G')
        m.dot = g
        o = Node('O')
        m.dash = o
        self.current = None
        self.word = ''

    def find_possible_characters(self, word):
        possible_characters = []
        # no more symbols left in the word
        if not word:
            return [self.current.character]
        symbol = word[0]
        word = word[1:]

        if symbol == '.':
            self.current = self.current.dot if self.current else self.dot
            possible_characters.extend(self.find_possible_characters(word))

        if symbol == '-':
            self.current = self.current.dash if self.current else self.dash
            possible_characters.extend(self.find_possible_characters(word))

        if symbol == '?':
            # Keep track of original current
            current = self.current

            self.current = current.dot if current else self.dot
            possible_characters.extend(self.find_possible_characters(word))

            self.current = current.dash if current else self.dash
            possible_characters.extend(self.find_possible_characters(word))
        return possible_characters


def possibilities(word):
    morse_code = MorseCode()
    return morse_code.find_possible_characters(word)


print(possibilities("."))  # ["E"])
print(possibilities(".-"))  # ["A"])
print(possibilities("?"))  # ["E","T"])
print(possibilities("?."))  # ["I","N"])
print(possibilities(".?"))  # ["I","A"])
