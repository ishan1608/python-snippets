def _recursive_counter(data, index, results):
    if (index >= (len(data) -1)):
        return 1

    if (results[index] != 0):
        return results[index]

    if (index + 1 < len(data)):
        number = data[index] * 10 + data[index + 1]
    else:
        number = data[index]

    firstResult = _recursive_counter(data, index + 1, results)
    secondResult = 0
    if ((number >= 10) and (number <= 26)):
        secondResult = _recursive_counter(data, index + 2, results)

    results[index] = firstResult + secondResult
    return results[index]


def recursive_possibilities_count(data):
    results = [0] * len(data)
    return _recursive_counter(data, 0, results)


while(True):
    input_str = input()
    if input_str[0] == '0':
        break
    # NOTE Even though it mentions that there will be no spaces, I am handling it as two separate test cases, because that's how the "Expected Correct Output" also works
    inputs = input_str.split(' ')
    for input_item in inputs:
        data = []
        for char in input_item:
            data.append(int(char))
        result = recursive_possibilities_count(data)
        print(result)
