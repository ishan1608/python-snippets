import json

locations = """ [
  {"id": 1, "name": "San Francisco Bay Area", "parent_id": null},
  {"id": 2, "name": "San Jose", "parent_id": 3},
  {"id": 3, "name": "South Bay", "parent_id": 1},
  {"id": 4, "name": "San Francisco", "parent_id": 1},
  {"id": 5, "name": "Manhattan", "parent_id": 6},
  {"id": 6, "name": "New York", "parent_id": null}
] """
locations = json.loads(locations)

# Storing city data in a map with parent_id as key
city_map_parent_wise = {}
for city in locations:
    parent_id = city['parent_id']
    # Create empty list for the new parent_id
    if parent_id not in city_map_parent_wise:
        city_map_parent_wise[parent_id] = []
    # Add city to the list of parent_id
    city_map_parent_wise[parent_id].append(city)

def print_city_info(parent_id=None, prefix=None):
    # Setting prefix as blank string in case of top level
    prefix = "" if prefix is None else prefix
    try:
        # using lambda as the cmp function which compares 'name' of the cities
        for city_data in sorted(city_map_parent_wise[parent_id], cmp=lambda x, y: -1 if x['name'] < y['name'] else 1):
            # Display the name of the city in proper format with prefix
            print '{}{}'.format(prefix, city_data['name'])
            # Recursively display the sub-cities
            print_city_info(parent_id=city_data['id'], prefix=prefix + '-')
    except KeyError:
        # There are no sub-cities
        pass

# Not providing parent_id as parent_id is None for top level cities
print_city_info()
