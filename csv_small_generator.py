import csv

with open('test.csv', 'w') as csv_file:
    row_writer = csv.writer(csv_file, quoting=csv.QUOTE_MINIMAL)

    row_writer.writerow([
        "Last Update Date",
        "Campaign",
        "<Campaign>_id",
        "Merchant ID",
        "Associate",
        "<Associate>_id",
        "Launch Date",
        "Launch Type",
        "Account Status",
        "Enforcement Date",
        "Enforcement Reason Code",
        "Enforcement Type",
        "Count of Enforcement",
        "No. of Appeals",
        "Reinstatement Date",
        "Merchant Name",
        "Bucket Date",
        "Bucket Ageing",
        "Merchant Email",
        "Total No. of Calls",
        "Next Follow Up Date",
        "Brand",
        "CIS_ID",
        "Flex Launched",
    ])

    merchant_id_offset = 23332194913
    for index in range(5000):
        merchant_id_offset = merchant_id_offset + index
        row_writer.writerow([
            "2018-07-31",
            "New Merchant Campaign",
            "ajfmxphoht",
            str(merchant_id_offset),
            "Ishan",
            "1",
            "",
            "",
            "Active",
            "",
            "",
            "",
            "",
            "",
            "",
            "MB.Traders",
            "",
            "",
            "foodsamrit@gmail.com",
            "0",
            "",
            "Active Elements",
            "22332194912Active Elements",
            "No",
        ])
