def SolveIt (str):
    highest_occurrence = 0
    character_map = {}
    for char in str:
        character_map[char] = character_map.get(char, 0) + 1
        if character_map[char] > highest_occurrence:
            highest_occurrence = character_map[char]

    highest_occurrence_characters = []
    for char in character_map:
        if character_map[char] == highest_occurrence:
            highest_occurrence_characters.append(char)

    highest_occurrence_characters.sort()
    return highest_occurrence_characters[0]

str = input()

out_ = SolveIt(str)
print (out_)