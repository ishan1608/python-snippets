number_of_tests = int(raw_input())
tests_limits = map(int, raw_input().split(' '))
if len(tests_limits) != number_of_tests:
    raise ValueError(u"Number of inputs doesn't match the number of test cases")


def print_fizz_buzz(limit):
    def print_single_number(number):
        divisible_by_3 = False if number % 3 != 0 else True
        divisible_by_5 = False if number % 5 != 0 else True
        if divisible_by_5 and divisible_by_3:
            print 'FizzBuzz'
        elif divisible_by_3:
            print 'Fizz'
        elif divisible_by_5:
            print 'Buzz'
        else:
            print number

    for i in xrange(1, limit + 1):
        print_single_number(i)


for test_limit in tests_limits:
    print_fizz_buzz(test_limit)
