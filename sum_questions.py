def find_min_difference(n, k, s):
    number_of_k_required = s / k
    extra_after_k = s % k
    number_of_k_available = (n * (n - 1)) / 2
    if extra_after_k == 0:
        if number_of_k_available >= number_of_k_required:
            return 0
        else:
            return (number_of_k_required - number_of_k_available) * k
    else:
        if number_of_k_available >= number_of_k_required:
            return extra_after_k
        else:
            return ((number_of_k_required - number_of_k_available) * k) + extra_after_k


number_of_tests = int(raw_input())
for _ in xrange(number_of_tests):
    n, k, q = map(int, raw_input().split())
    out_ = []
    for __ in xrange(q):
        s = int(raw_input())
        out_.append(find_min_difference(n, k, s))
    print ' '.join(map(str, out_))
