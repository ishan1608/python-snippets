def find_duplicate_2(input_arr):
    def _swap_numbers(arr, i):
        number = arr[i]
        position = i + 1
        # Number is in it's position
        if number == position:
            return None

        # Number not in it's position
        if number != position:
            targetPositionNumber = arr[number - 1]
            # The number at the position is the same number in the current position, so it must be the duplicate
            if number == targetPositionNumber:
                return number

            # Swap numbers
            arr[number - 1], arr[i] = number, targetPositionNumber

            # New numbers
            number = arr[i]
            if number != position:
                return _swap_numbers(arr, i)

    duplicate = None
    for i in xrange(len(input_arr)):
        duplicate = _swap_numbers(input_arr, i)
        if duplicate:
            break
    return duplicate

arr_data = [3, 2, 5, 4, 4]
print 'Duplicate item in [{}] is {}'.format(arr_data, find_duplicate_2(arr_data))
arr_data = [3, 2, 1, 5, 1]
print 'Duplicate item in [{}] is {}'.format(arr_data, find_duplicate_2(arr_data))
arr_data = [1, 1, 2, 2, 3]
print 'Duplicate item in [{}] is {}'.format(arr_data, find_duplicate_2(arr_data))
